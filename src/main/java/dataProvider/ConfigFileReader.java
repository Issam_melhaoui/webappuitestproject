package dataProvider;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import enums.DriverType;
import enums.EnvironmentType;
import enums.RemoteDriverType;

public class ConfigFileReader {

	private Properties properties;
	private String projectPath = System.getProperty("user.dir");
	private final String propertyFilePath = projectPath+"\\src\\test\\resources\\Configs\\Configuation.properties";


	public ConfigFileReader(){
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try { properties.load(reader); }
			catch (IOException e) { e.printStackTrace(); }
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Properties file not found at path : " + propertyFilePath);
		}finally {
			try { if(reader != null) reader.close(); }
			catch (IOException ignore) {}
		}
	}


	public long getImplicitlyWait() {		
		String implicitlyWait = properties.getProperty("implicitlyWait");
		if(implicitlyWait != null) {
			try{
				return Long.parseLong(implicitlyWait);
			}catch(NumberFormatException e) {
				throw new RuntimeException("Not able to parse value : " + implicitlyWait + " in to Long");
			}
		}
		return 30;		
	}
	
	public long getExplicitWait() {		
		String explicitWait = properties.getProperty("explicitWait");
		if(explicitWait != null) {
			try{
				return Long.parseLong(explicitWait);
			}catch(NumberFormatException e) {
				throw new RuntimeException("Not able to parse value : " + explicitWait + " in to Long");
			}
		}
		return 30;		
	}

	public String getApplicationUrl() {
		String url = properties.getProperty("URL");
		if(url != null) return url;
		else throw new RuntimeException("Application Url not specified in the Configuration.properties file for the Key:url");
	}

	public String getEmail() {
		String login = properties.getProperty("email");
		if(login != null) return login;
		else throw new RuntimeException("Application Email not specified in the Configuration.properties file for the Key:email");
	}
	

	public String getPassword() {
		String password = properties.getProperty("password");
		if(password != null) return password;
		else throw new RuntimeException("Application password not specified in the Configuration.properties file for the Key:password");
	}
	
	public String getAutomateUsername() {
		String automate_username = properties.getProperty("AUTOMATE_USERNAME");
		if(automate_username != null) return automate_username;
		else throw new RuntimeException("BrowserStack automate_username not specified in the Configuration.properties file for the Key:automate_username");
	}
	
	public String getAutomateAcccesKey() {
		String automate_access_key = properties.getProperty("AUTOMATE_ACCESS_KEY");
		if(automate_access_key != null) return automate_access_key;
		else throw new RuntimeException("BrowserStack automate_access_key not specified in the Configuration.properties file for the Key:automate_access_key");
	}


	public DriverType getBrowser() {
		String browserName = properties.getProperty("browser");
		if(browserName == null || browserName.equals("chrome")) return DriverType.CHROME;
		else if(browserName.equalsIgnoreCase("firefox")) return DriverType.FIREFOX;
		else if(browserName.equals("medge")) return DriverType.MICROSOFTEDGE;
		else throw new RuntimeException("Browser Name Key value in Configuration.properties is not matched : " + browserName);
	}
	
	public RemoteDriverType getRemoteBrowser() {
		String remoteBrowserName = properties.getProperty("remotebrowser");
		if(remoteBrowserName == null || remoteBrowserName.equals("chrome")) return RemoteDriverType.CHROME;
		else if(remoteBrowserName.equalsIgnoreCase("firefox")) return RemoteDriverType.FIREFOX;
		else if(remoteBrowserName.equals("safari")) return RemoteDriverType.SAFARI;
		else throw new RuntimeException("Remote Browser Name Key value in Configuration.properties is not matched : " + remoteBrowserName);
	}

	public EnvironmentType getEnvironment() {
		String environmentName = properties.getProperty("environment");
		if(environmentName == null || environmentName.equalsIgnoreCase("local")) return EnvironmentType.LOCAL;
		else if(environmentName.equals("remote")) return EnvironmentType.REMOTE;
		else throw new RuntimeException("Environment Type Key value in Configuration.properties is not matched : " + environmentName);
	}

	public Boolean getBrowserWindowSize() {
		String windowSize = properties.getProperty("windowMaximize");
		if(windowSize != null) return Boolean.valueOf(windowSize);
		return true;
	}


}