package managers;

import org.openqa.selenium.WebDriver;

import pageObjects.Login;


public class PageObjectManager {
	
	private WebDriver driver;
	private Login login;
	
	public PageObjectManager(WebDriver driver) {
		this.driver = driver;
	}
	
	public Login getHomePage(){
		return (login == null) ? login = new Login(driver) : login;
	}


}
