package managers;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import enums.DriverType;
import enums.EnvironmentType;
import enums.RemoteDriverType;
import io.github.bonigarcia.wdm.WebDriverManager;


public class WebDriverManage {
	
	private WebDriver driver;
	private static DriverType driverType;
	private static RemoteDriverType remoteDriverType;
	private static EnvironmentType environmentType;
	private static  String AUTOMATE_USERNAME = FileReaderManager.getInstance().getConfigReader().getAutomateUsername();
	private static  String AUTOMATE_ACCESS_KEY = FileReaderManager.getInstance().getConfigReader().getAutomateAcccesKey();
	private static  String URL = "https://" + AUTOMATE_USERNAME + ":" + AUTOMATE_ACCESS_KEY + "@hub-cloud.browserstack.com/wd/hub";
	
	public WebDriverManage() {
		remoteDriverType = FileReaderManager.getInstance().getConfigReader().getRemoteBrowser();
		driverType = FileReaderManager.getInstance().getConfigReader().getBrowser();
		environmentType = FileReaderManager.getInstance().getConfigReader().getEnvironment();
	}
	
	public WebDriver getDriver() {
		if(driver == null) driver = createDriver();
		return driver;
	}
	
	private WebDriver createDriver() {
		   switch (environmentType) {	    
	        case LOCAL : driver = createLocalDriver();
	        	break;
	        case REMOTE : driver = createRemoteDriver();
	        	break;
		   }
		   return driver;
	}

	
	private WebDriver createRemoteDriver() {

	    	DesiredCapabilities caps = new DesiredCapabilities();
	    	
			switch (remoteDriverType) {
			case SAFARI :
			    caps.setCapability("os_version", "Big Sur");
			    caps.setCapability("resolution", "1920x1080");
			    caps.setCapability("browser", "Safari");
			    caps.setCapability("browser_version", "14.0");
			    caps.setCapability("os", "OS X");
			    caps.setCapability("name", "Cloud Test on MAC OS and Safari browser"); // test name
				try {
					driver = new RemoteWebDriver(new URL(URL), caps);
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				break;
				
	        case CHROME : 
	            caps.setCapability("os_version", "10");
	            caps.setCapability("resolution", "1280x1024");
	            caps.setCapability("browser", "Chrome");
	            caps.setCapability("browser_version", "latest");
	            caps.setCapability("os", "Windows");
	            caps.setCapability("name", "Cloud Test on Windows OS and Chrome browser"); // test name
				try {
					driver = new RemoteWebDriver(new URL(URL), caps);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	break;
	        case FIREFOX :
	            caps.setCapability("os_version", "10");
	            caps.setCapability("resolution", "1920x1080");
	            caps.setCapability("browser", "Firefox");
	            caps.setCapability("browser_version", "latest");
	            caps.setCapability("os", "Windows");
	            caps.setCapability("name", "Cloud Test on Windows OS and Firefox browser"); // test name
	            try {
					driver = new RemoteWebDriver(new URL(URL), caps);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		break;
			}
	        if(FileReaderManager.getInstance().getConfigReader().getBrowserWindowSize()) driver.manage().window().maximize();
	        driver.manage().timeouts().implicitlyWait(FileReaderManager.getInstance().getConfigReader().getImplicitlyWait(), TimeUnit.SECONDS);

		    return driver;
	}
	
	private WebDriver createLocalDriver() {
        switch (driverType) {	    
        case FIREFOX : 
        	WebDriverManager.firefoxdriver().setup();
        	driver = new FirefoxDriver();
	    	break;
        case CHROME :
        	WebDriverManager.chromedriver().setup();
        	driver = new ChromeDriver();
    		break;
        case MICROSOFTEDGE : 
        	WebDriverManager.edgedriver().setup();
        	driver = new EdgeDriver();
    		break;
        }
        
        if(FileReaderManager.getInstance().getConfigReader().getBrowserWindowSize()) driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(FileReaderManager.getInstance().getConfigReader().getImplicitlyWait(), TimeUnit.SECONDS);
		return driver;
	}	
	
	public void quitDriver() {
		driver.close();
		driver.quit();		
	}
	
	public void getImplicitWait() {
		driver.manage().timeouts().implicitlyWait(FileReaderManager.getInstance().getConfigReader().getImplicitlyWait(), TimeUnit.SECONDS);
	}

}