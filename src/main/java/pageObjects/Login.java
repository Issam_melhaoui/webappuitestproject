package pageObjects;

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import managers.FileReaderManager;

public class Login extends BasePage{
	
	
	By loginPopup = By.id("openModalConnection");
	By Email = By.id("connectionForm-username");
	By Password = By.id("connectionForm-password");
	By Connexion = By.xpath("//*[@id=\"connectionForm\"]/div[5]/button");
	By SelectMenu = By.id("dropdownUserMenu");
	By Deconnexion = By.xpath("//a[@href ='/logout']");
	By ErrorMessage = By.xpath("//div[@class='alert alert-danger']");
    
	
    public Login(WebDriver driver){

        this.driver = driver;

    }

    
    public void ClickOnLoginPopup(){

         driver.findElement(loginPopup).click();

    }
    
    
    public void SetEmail(){

        driver.findElement(Email).sendKeys(FileReaderManager.getInstance().getConfigReader().getEmail());

   }
    
    
	public String decodePassword(String password){
		
		byte[] decodedString = Base64.decodeBase64(password);
		return new String(decodedString);
		
	}
    

    public void SetValidPassword(){
    	
    	String password_decode = decodePassword(FileReaderManager.getInstance().getConfigReader().getPassword());
        driver.findElement(Password).sendKeys(password_decode);

    }
    
    public void SetInValidPassword(){
    	
        driver.findElement(Password).sendKeys(FileReaderManager.getInstance().getConfigReader().getPassword());

    }

    
    public void ClickOnConnexion(){

    	driver.findElement(Connexion).click();

    }
    
    public void ClickOnDeconnexion(){
    	
    	driver.findElement(SelectMenu).click();
        driver.findElement(Deconnexion).click();

    }
    
    public String CheckErrorMessage(){
    	
    	WebDriverWait wait = new WebDriverWait(driver, FileReaderManager.getInstance().getConfigReader().getExplicitWait());
		WebElement WaitCondition; 
		WaitCondition = wait.until(ExpectedConditions.visibilityOfElementLocated(ErrorMessage));
    	return WaitCondition.getText();

    }
 
    
	public void NavigateToSmoodPage() {
		
		driver.get(FileReaderManager.getInstance().getConfigReader().getApplicationUrl());
		
	}
	
	

}
