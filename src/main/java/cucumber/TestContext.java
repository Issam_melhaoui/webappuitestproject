package cucumber;

import managers.PageObjectManager;
import managers.WebDriverManage;

public class TestContext {
	
	 private WebDriverManage webDriverManage;
	 private PageObjectManager pageObjectManager;
	 
	 public TestContext(){
	 webDriverManage = new WebDriverManage();
	 pageObjectManager = new PageObjectManager(webDriverManage.getDriver());
	 }
	 
	 public WebDriverManage getWebDriverManager() {
	 return webDriverManage;
	 }
	 
	 public PageObjectManager getPageObjectManager() {
	 return pageObjectManager;
	 }
	 
	 
}