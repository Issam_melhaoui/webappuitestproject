#Author: Issameddine MELHAOUI
@loginTest
Feature: Feature to test login functionality of smood

  Background: 
    Given user is on login smood popup

  @successfulLogin
  Scenario: Check login is successful with valid credentials
    Given user enters valid email and password
    When click on connexion
    Then user should Login into smood

  @unSuccessfulLogin
  Scenario: Check login is unsuccessful with invalid credentials
    Given user enters invalid email and password
    When click on connexion
    Then user should not Login into smood
