package testRunners;


import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/Features", glue={"com.stepDefintion.app"},
monochrome=true,
plugin={"pretty","json:target/cucumber-report/report.json",
"html:target/cucumber-report/cucumber.html",
"junit:target/cucumber-report/cucumber.xml"
},
tags="@loginTest"
)

public class TestRunner {

}
