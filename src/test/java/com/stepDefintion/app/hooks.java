package com.stepDefintion.app;


import cucumber.TestContext;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;


public class hooks {
	
	 TestContext testContext;
	 
	 public hooks(TestContext context) {
	 testContext = context;
	 }
	 
	@Before
	public void BeforeSteps() {
		
	 }
	 
	@After(order = 1)
	public void afterScenario(Scenario scenario) {
        if (scenario.isFailed()) {
            scenario.log("Scenario failed so capturing a screenshot");

            TakesScreenshot screenshot = (TakesScreenshot) testContext.getWebDriverManager().getDriver();
            scenario.attach(screenshot.getScreenshotAs(OutputType.BYTES), "image/png", scenario.getName());
        }

		}
		
		
	@After(order = 0)
	public void AfterSteps() {
			testContext.getWebDriverManager().quitDriver();
		}
	

}
