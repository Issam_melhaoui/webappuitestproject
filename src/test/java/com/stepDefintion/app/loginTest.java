package com.stepDefintion.app;


import org.junit.Assert;

import cucumber.TestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.Login;


public class loginTest{

	Login login;
	TestContext testContext;


	public loginTest(TestContext context) {
		testContext = context;
		login = testContext.getPageObjectManager().getHomePage();
	}


	@Given("user is on login smood popup")
	public void user_is_on_login_smood_popup() {

		login.NavigateToSmoodPage();
		login.ClickOnLoginPopup();

	}

	@Given("user enters valid email and password")
	public void user_enters_valid_email_and_password() {

		login.SetEmail();
		login.SetValidPassword();

	}

	@When("click on connexion")
	public void click_on_connexion() {

		login.ClickOnConnexion();

	}
	
	@Then("user should Login into smood")
	public void user_should_Login_into_smood() {

		login.ClickOnDeconnexion();

	}
	
	@Given("user enters invalid email and password")
	public void user_enters_invalid_email_and_password() {

		login.SetEmail();
		login.SetInValidPassword();

	}
	
	@Then("user should not Login into smood")
	public void user_should_not_Login_into_smood() {
		String ErrorMessage = login.CheckErrorMessage();
		Assert.assertEquals("L'email et/ou le mot de passe n'est pas valide.", ErrorMessage);

	}
	
}
